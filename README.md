# Atlassian Bamboo Bitbucket Jira Confluence Crowd Fisheye/Cruicible Helm Chart

## Pre-Reqs

* Kubernetes cluster
* kubectl with configured context
* helm (tested with 3.x only)
* available persistent volumes (7 PVs)
* Nginx ingress controller (optional)

## How to Deploy

```
# create bamboo namespace
kubectl create namespace atlassian

# install Bamboo with default settings
helm install atlassian ./ --set bamboo.mode.install=true --set bitbucket.mode.install=true --set jira.mode.install=true -n atlassian

# watch install job logs
# you may need to wait until the job pod is in the running state
kubectl logs -f job/install-bamboo -n bamboo
```

## What Can Be Configured

| Parameter        | Description      | Default Value |
| -----------------|:-------------:| |
| bamboo.mode.install     | run initial installation, connect to DB, create tables. Set to true only during the 1st run | false         |
| bamboo.image.repository | image repo      |  dchevell/bamboo |
| bamboo.image.tag | are neat      | see values.yaml  |
| bamboo.image.pullPolicy | image pull policy      | IfNotPresent  |
| bamboo.license          | Bamboo license. Please mind indentation, see example in values.yaml      | none  |
| bamboo.serverId | your Bamboo server ID which comes with Bamboo license     | none  |
| bamboo.serverKey | your Bamboo server key      | 278  |
| bamboo.adminUser | your Bamboo admin user      | admin  |
| bamboo.adminPassword | your Bamboo admin password      | password  |
| bamboo.adminFullName | your Bamboo admin full name      | Bamboo+Admin  |
| bamboo.adminEmail | your Bamboo admin email      | admin@atlassian.com  |
| bamboo.db.type | database type [ha|postgres]     | postgres  |
| bamboo.db.deployPostgres | deploy Postgres or connect to existing database   | true  |
| bamboo.db.host | database host     | postgres  |
| bamboo.db.port | database port    | 5432  |
| bamboo.db.username | database username    | bamboo  |
| bamboo.db.password | database password     | password  |
| bamboo.db.database | database name     | bamboo  |
| bamboo.ingress.enabled | create ingress     | true  |
| bamboo.ingress.annotations.ingressClass | ingressClass     | nginx  |
| bamboo.ingress.host | hostname for ingress spec     | none  |
| bamboo.resources.limits.cpu | CPU resource limits for Bamboo container     | 3000m  |
| bamboo.resources.limits.memory | RAM resource limits for Bamboo container     | 4Gi  |
| bamboo.resources.requests.cpu | CPU resource requests for Bamboo container     | 100m  |
| bamboo.resources.requests.memory | RAM resource requests for Bamboo container     | 2Gi  |
| bamboo.javaOpts.jvmMinimumMemory | xms for Bamboo     | 1024m  |
| bamboo.javaOpts.jvmMaximumMemory | xmx for Bamboo     | 4096m  |

Custom values can be passed in helm commands as `--set key=value`, for example `--set image.tag=custom`, or you can edit *values.yaml* before installing this Helm chart.

## How Installation Works

An init container is added to Bamboo deployment when `mode.install` is enabled. This init container mounts a ConfigMap with the initial `bamboo.cfg.xml` and copies it to a mounted directory.

If `mode.install` is enabled a K8s job is created to install apps. This job runs a pod with a mounted ConfigMap that holds an installation script. The script makes a series of http requests against Bamboo internal endpoint to validate license, connect to a database of choice and create an admin user.

## TODOs

* better explore Python entrypoint to make use of built-in automation in the image
* make more things configurable  (remote agent, JAVA_OPTS etc)
* replace bash installer with a Golang or Python (Jira is left, Bamboo and Bitbucket install pods use Python entrypoints)
* make service type configurable, i.e. enable NodePort
* make PVCs configurable, allow adding annotations, storage classes etc. to make it possible to deploy to public clouds
* TLS/SSL support both on ingress and server level
