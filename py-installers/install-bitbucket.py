import requests
import time
import os
import re
import sys
import json

protocol = os.getenv('PROTOCOL')
bitbucketInternalURL = os.getenv('BITBUCKET_ENDPOINT')
bambooInternalURL = os.getenv('BAMBOO_ENDPOINT')

key = os.getenv('BITBUCKET_LICENSE')
adminUser = os.getenv('ADMIN_USER')
adminPassword = os.getenv('ADMIN_PASSWORD')
bambooAdminUser = os.getenv('BAMBOO_ADMIN_USER')
bambooAdminPassword = os.getenv('BAMBOO_ADMIN_PASSWORD')
email = os.getenv('ADMIN_EMAIL')
fullName = os.getenv('FULL_NAME')
bitbucketURL = os.getenv('BITBUCKETURL')
bambooURL = os.getenv('BAMBOOURL')
# wait timeout, 7 min to let k8s pull image, start pod etc
timeout = time.time() + 60 * 7

# connection = None
# while connection is None:
#     if time.time() > timeout:
#         print('Timeout reached. Bitbucket is not responding. Check logs')
#         sys.exit('Exiting')
#     try:
#         connection = requests.get(protocol + '://' + bitbucketInternalURL)
#     except:
#         print("Bitbucket hostname cannot be yet resolved")
#         time.sleep(5)
#
# response = requests.get(protocol + '://' + bitbucketInternalURL)
# if response.status_code == 200:
#     print('Bitbucket is ready, response code:', response.status_code)
# # wait for Bamboo to start responding
# while response.status_code != 200:
#     if time.time() > timeout:
#         print('Timeout reached. Bitbucket is not responding. Check logs')
#         sys.exit('Exiting')
#     print('Bitbucket is not ready, response code:', response.status_code)
#     response = requests.get(protocol + '://' + bitbucketInternalURL)
#     time.sleep(5)
#
# response = requests.get(protocol + '://' + bitbucketInternalURL)
# redirect = response.history[0].headers['Location']
# # wait for Bitbucket to be ready to handle license, database config and admin user. Waiting for
# while 'setup' not in redirect:
#     if time.time() > timeout:
#         print('Timeout reached. Bitbucket is not ready. Check logs')
#         sys.exit('Exiting')
#     response = requests.get(protocol + '://' + bitbucketInternalURL)
#     redirect = response.history[0].headers['Location']
#     print('Waiting for Bitbucket to be ready. Current redirect is ' + response.history[0].headers['Location'])
#     time.sleep(15)
#
# session = requests.get(protocol + '://' + bitbucketInternalURL + '/setup').headers
# m = re.search('BITBUCKETSESSIONID=(.+?);', str(session))
# session = m.group(1)
# print('BITBUCKETSESSIONID: ' + session)
# headers = {'Cookie': 'BITBUCKETSESSIONID=' + session + ';'}
# token = requests.get(protocol + '://' + bitbucketInternalURL + '/setup', headers=headers).content
#
# m = re.search('"atl_token" value="(.+?)"', str(token))
# token = m.group(1)
# print('atl_token: ' + token)
#
# licenseData = {'step': 'settings',
#                'applicationTitle': 'Bitbucket',
#                'baseUrl': bitbucketURL,
#                'license-type': 'false',
#                'license': key,
#                'licenseDisplay': key,
#                'submit': 'next',
#                'atl_token': token
#                }
# headers = {'Cookie': 'BITBUCKETSESSIONID=' + session + ';'}
#
# print('Validating license')
# validateLicense = requests.post(protocol + '://' + bitbucketInternalURL + '/setup', data=licenseData, headers=headers)
#
# time.sleep(10)
#
# userData = {'step': 'user',
#             'username': adminUser,
#             'password': adminPassword,
#             'confirmPassword': adminPassword,
#             'fullname': fullName,
#             'email': email,
#             'skipJira': 'Go to Bitbucket',
#             'atl_token': token
#             }
# print('Creating admin user')
#
# createUser = requests.post(protocol + '://' + bitbucketInternalURL + '/setup', data=userData, headers=headers)

# create application links between Bamboo and Bitbucket
# wait for Bamboo to start responding

connection = None
while connection is None:
    if time.time() > timeout:
        print('Timeout reached. Bamboo is not responding')
        sys.exit('Exiting')
    try:
        connection = requests.get(protocol + '://' + bambooInternalURL)
    except:
        print("Bamboo hostname cannot be yet resolved")
        time.sleep(5)

response = requests.get(protocol + '://' + bambooInternalURL)

while response.status_code != 200:
    if time.time() > timeout:
        print('Timeout reached. Bamboo is not responding. Application links will not be created')
        sys.exit('Exiting')
    response = requests.get(protocol + '://' + bambooInternalURL)
    print('Bamboo is not ready, response code:', response.status_code)
    time.sleep(5)

print('Bamboo is ready, response code:', response.status_code)

print(requests.get(protocol + '://' + bambooInternalURL).headers)
sys.exit('Exiting')
getHeaders = requests.get(protocol + '://' + bambooInternalURL).headers['Set-Cookie']
m = re.search('atl.xsrf.token=(.+?);', str(getHeaders))
bambooAtlToken = m.group(1)
print('Bamboo token: ' + bambooAtlToken)
m = re.search('JSESSIONID=(.+?);', str(getHeaders))
jsessionid = m.group(1)

headers = {'Cookie': 'JSESSIONID=' + jsessionid + '; atl.xsrf.token=' + bambooAtlToken}

bambooLoginData = {
    'os_destination': '/start.action',
    'os_username': bambooAdminUser,
    'os_password': bambooAdminPassword,
    'checkBoxFields': 'os_cookie',
    'save': 'Log in',
    'atl_token': bambooAtlToken
}
login = requests.post(protocol + '://' + bambooInternalURL + '/userlogin.action', data=bambooLoginData,
                      headers=headers)
m = re.search('JSESSIONID=(.+?);', str(login))
jsessionid = m.group(1)
print('Bamboo auth session: ' + jsessionid)
bambooHeaders = {'Cookie': 'JSESSIONID=' + jsessionid + '; atl.xsrf.token=' + bambooAtlToken}
bambooAppID = requests.get(
    protocol + '://' + bambooInternalURL + '/rest/applinks/3.0/applicationlinkForm/manifest.json?url=' + bitbucketURL + '&_=1591014774443',
    headers=bambooHeaders).content
bambooAppID = json.loads(bambooAppID)['id']
print('Bamboo Application ID: ' + bambooAppID)

bitbucketLoginData = {
    'j_username': 'admin',
    'j_password': 'password',
    '_atl_remember_me': 'on',
    'submit': 'Log in'
}

bitbucketSessionId = \
    requests.post(protocol + '://' + bitbucketInternalURL + '/j_atl_security_check', data=bitbucketLoginData).history[
        0].headers['Set-Cookie']
m = re.search('BITBUCKETSESSIONID=(.+?);', str(bitbucketSessionId))
bitbucketSessionId = m.group(1)
print('Bitbucket auth session: ' + bitbucketSessionId)

bambooAppLinkData = {
    'id': bambooAppID,
    'name': 'Bitbucket',
    'rpcUrl': bitbucketURL,
    'displayUrl': bitbucketURL,
    'typeId': 'stash'
}

bambooHeaders = {'Cookie': 'JSESSIONID=' + jsessionid + '; atl.xsrf.token=' + bambooAtlToken,
                 'Content-Type': 'application/json'}
print('Creating Bamboo App link ' + bambooAppID)
createBambooLink = requests.put(protocol + '://' + bambooInternalURL + '/rest/applinks/3.0/applicationlink',
                                json=bambooAppLinkData, headers=bambooHeaders)

print('Configuring Bamboo App link ' + bambooAppID)

configureBambooAppLink = {
    "twoLOAllowed": 'true',
    "executingTwoLOUser": 'null',
    "twoLOImpersonationAllowed": 'false'
}
configureBambooLink = requests.put(
    protocol + '://' + bambooInternalURL + '/rest/applinks-oauth/1.0/applicationlink/' + bambooAppID + '/authentication/consumer?autoConfigure=true',
    json=configureBambooAppLink, headers=bambooHeaders)

bitbucketHeaders = {'Cookie': 'BITBUCKETSESSIONID=' + bitbucketSessionId + ';', 'Content-Type': 'application/json'}

bitbucketAppID = requests.get(
    protocol + '://' + bitbucketInternalURL + '/rest/applinks/3.0/applicationlinkForm/manifest.json?url=' + bambooURL + '&_=1591033313981',
    headers=bitbucketHeaders).content
bitbucketAppID = json.loads(bitbucketAppID)['id']

print('Bitbucket Application ID: ' + bitbucketAppID)

bitbucketAppLinkData = {
    'id': bitbucketAppID,
    'name': 'Atlassian Bamboo',
    'rpcUrl': bambooURL,
    'displayUrl': bambooURL,
    'typeId': 'bamboo'
}

print('Creating Bitbucket App link ' + bitbucketAppID)
createBitbucketLink = requests.put(protocol + '://' + bitbucketInternalURL + '/rest/applinks/3.0/applicationlink',
                                   json=bitbucketAppLinkData, headers=bitbucketHeaders)

print('Configuring oAuth provider for Bitbucket App link ' + bitbucketAppID)

jsonoauthProvider = {'config': {},
                     'provider': 'com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider'
                     }
configureBitbucketoAuthProvider = requests.put(
    protocol + '://' + bitbucketInternalURL + '/rest/applinks/3.0/applicationlink/' + bitbucketAppID + '/authentication/provider',
    json=jsonoauthProvider, headers=bitbucketHeaders)

json = {'twoLOAllowed': 'true',
        'executingTwoLOUser': 'null',
        'twoLOImpersonationAllowed': 'false'}

configureBitbucketoAuthConsumer = requests.put(
    protocol + '://' + bitbucketInternalURL + '/rest/applinks-oauth/1.0/applicationlink/' + bitbucketAppID + '/authentication/consumer?autoConfigure=true',
    json=json, headers=bitbucketHeaders)

jsonTwoLegged = {'config': {},
                 'provider': 'com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider'
                 }
configureBitbucketoTwoLegged = requests.put(
    protocol + '://' + bitbucketInternalURL + '/rest/applinks/3.0/applicationlink/' + bitbucketAppID + '/authentication/provider',
    json=jsonTwoLegged, headers=bitbucketHeaders)

print('Configuring oAuth provider for Bamboo App link ' + bambooAppID)


configureBamboooAuthProvider = requests.put(
    protocol + '://' + bambooInternalURL + '/rest/applinks/3.0/applicationlink/' + bambooAppID + '/authentication/provider',
    json=jsonTwoLegged, headers=bambooHeaders)

configureBambooTwoLegged = requests.put(
    protocol + '://' + bambooInternalURL + '/rest/applinks/3.0/applicationlink/' + bambooAppID + '/authentication/provider',
    json=jsonoauthProvider, headers=bambooHeaders)
