apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: bitbucket-data
  labels:
    k8s-app: bitbucket
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: bitbucket
  labels:
    k8s-app: bitbucket
  annotations:
    kubernetes.io/ingress.class: {{ .Values.bitbucket.ingress.annotations.ingressClass }}
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
spec:
  tls:
  - hosts:
    - "{{ .Values.bitbucket.ingress.host }}"
    secretName: bitbucket-tls
  rules:
  - host: "{{ .Values.bitbucket.ingress.host }}"
    http:
      paths:
      - path: /
        backend:
          serviceName: bitbucket
          servicePort: 7990
---
kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: bitbucket
  name: bitbucket
spec:
  type: NodePort
  ports:
  - name: web
    port: 7990
  - name: ssh
    port: 7999
    nodePort: 30081
  - name: debug
    port: 7896
  selector:
    k8s-app: bitbucket
---
kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: bitbucket
  name: bitbucket
spec:
  selector:
    matchLabels:
      k8s-app: bitbucket
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        k8s-app: bitbucket
    spec:
      initContainers:
      - name: check-pg-ready
        image: postgres:9.6-alpine
        command: ['sh', '-c',
          'until pg_isready -h {{ .Values.bitbucket.db.host }} -p 5432;
          do echo waiting for database; sleep 2; done;
          {{ if .Values.bitbucket.db.createDb }}PGPASSWORD="{{ .Values.bamboo.db.password }}" psql -h postgres -U {{ .Values.bamboo.db.username }} -c "create user {{ .Values.bitbucket.db.username }} with password ''{{ .Values.bitbucket.db.password }}''" || true;
          PGPASSWORD="{{ .Values.bamboo.db.password }}" psql -h postgres -U {{ .Values.bamboo.db.username }}  -c "create database {{ .Values.bitbucket.db.database }}" || true;
          PGPASSWORD="{{ .Values.bamboo.db.password }}" psql -h postgres -U {{ .Values.bamboo.db.username }}  -c "grant all privileges on database {{ .Values.bitbucket.db.database }} to {{ .Values.bitbucket.db.username }}" || true {{ end }}']
      containers:
      - name: bitbucket
        image: {{ .Values.bitbucket.image.repository }}:{{ .Values.bitbucket.image.tag }}
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 7990
          protocol: TCP
        - containerPort: 7999
          protocol: TCP
        - containerPort: 7896
          protocol: TCP
        env:
          - name: JDBC_DRIVER
            value: org.postgresql.Driver
          - name: JDBC_USER
            value: {{ .Values.bitbucket.db.username }}
          - name: JDBC_PASSWORD
            value: {{ .Values.bitbucket.db.password }}
          - name: JDBC_URL
            value: jdbc:postgresql://{{ .Values.bitbucket.db.host }}:{{ .Values.bitbucket.db.port }}/{{ .Values.bitbucket.db.database }}
          - name: JVM_MINIMUM_MEMORY
            value: {{ .Values.bitbucket.javaOpts.jvmMinimumMemory }}
          - name: JVM_MAXIMUM_MEMORY
            value: {{ .Values.bamboo.javaOpts.jvmMaximumMemory }}
          - name: JVM_SUPPORT_RECOMMENDED_ARGS
            value: "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=7896"
        volumeMounts:
        - name: bitbucket-data
          mountPath: /var/atlassian/application-data/bitbucket
        - name: bitbucket-properties
          mountPath: /var/atlassian/application-data/bitbucket/shared/bitbucket.properties
          subPath: bitbucket.properties
        readinessProbe:
          tcpSocket:
            port: 7990
          initialDelaySeconds: 15
          periodSeconds: 5
          failureThreshold: 25
        resources:
          requests:
            memory: "2Gi"
            cpu: "100m"
          limits:
            memory: "4Gi"
            cpu: "3000m"
      volumes:
      - name: bitbucket-data
        persistentVolumeClaim:
          claimName: bitbucket-data
      - name: bitbucket-properties
        configMap:
          name: bitbucket-properties
