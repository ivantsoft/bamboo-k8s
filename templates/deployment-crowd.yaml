apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: crowd-data
  labels:
    k8s-app: crowd
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: crowd
  labels:
    k8s-app: crowd
  annotations:
    kubernetes.io/ingress.class: {{ .Values.crowd.ingress.annotations.ingressClass }}
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/proxy-connect-timeout: "60000"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "60000"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "6000"

spec:
  tls:
  - hosts:
    - "{{ .Values.crowd.ingress.host }}"
    secretName: crowd-tls
  rules:
  - host: "{{ .Values.crowd.ingress.host }}"
    http:
      paths:
      - path: /
        backend:
          serviceName: crowd
          servicePort: 8095
---
kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: crowd
  name: crowd
spec:
  type: NodePort
  ports:
  - name: web
    port: 8095
    nodePort: 30088
  selector:
    k8s-app: crowd
---
kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: crowd
  name: crowd
spec:
  selector:
    matchLabels:
      k8s-app: crowd
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        k8s-app: crowd
    spec:
      initContainers:
      - name: check-pg-ready
        image: postgres:9.6-alpine
        command: ['sh', '-c',
          'until pg_isready -h postgres -p 5432;
          do echo waiting for database; sleep 2; done;
          {{ if .Values.crowd.db.createDb }}PGPASSWORD="{{ .Values.bamboo.db.password }}" psql -h postgres -U {{ .Values.bamboo.db.username }} -c "create user {{ .Values.crowd.db.username }} with password ''{{ .Values.crowd.db.password }}''" || true;
          PGPASSWORD="{{ .Values.bamboo.db.password }}" psql -h postgres -U {{ .Values.bamboo.db.username }}  -c "create database {{ .Values.crowd.db.database }}" || true;
          PGPASSWORD="{{ .Values.bamboo.db.password }}" psql -h postgres -U {{ .Values.bamboo.db.username }}  -c "grant all privileges on database {{ .Values.crowd.db.database }} to {{ .Values.crowd.db.username }}" || true {{ end }}']
      containers:
      - name: crowd
        image: {{ .Values.crowd.image.repository }}:{{ .Values.crowd.image.tag }}
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 8095
          protocol: TCP
        env:
          - name: ATL_PROXY_NAME
            value: {{ .Values.crowd.ingress.host }}
          - name: ATL_PROXY_PORT
            value: "443"
          - name: ATL_TOMCAT_SCHEME
            value: https
          - name: JVM_MINIMUM_MEMORY
            value: {{ .Values.crowd.javaOpts.jvmMinimumMemory }}
          - name: JVM_MAXIMUM_MEMORY
            value: {{ .Values.crowd.javaOpts.jvmMaximumMemory }}
        volumeMounts:
        - name: crowd-data
          mountPath: /var/atlassian/application-data/crowd
        readinessProbe:
          tcpSocket:
            port: 8095
          initialDelaySeconds: 15
          periodSeconds: 5
          failureThreshold: 25
        resources:
          requests:
            memory: "2Gi"
            cpu: "100m"
          limits:
            memory: "4Gi"
            cpu: "3000m"
      volumes:
      - name: crowd-data
        persistentVolumeClaim:
          claimName: crowd-data
